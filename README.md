# SchedulerLite
任务调度系统，基于HTTP的外部调度系统，零浸入，包括对基于cron的定时任务和基于时间轮的延迟任务的调用。

## 项目说明

 1. SchedulerLite.Service 控制台程序，作为服务运行，调用任务
 2. SchedulerLite.Manager UI管理界面，录入任务，查看日志


## 简单配置
```xml
  <appSettings>
    <clear/>
    <!--加载定时任务的秒数间隔，0为不启用-->
    <add key="loadTimedTaskIntervalSeconds" value="5"/>
    <!--加载延迟任务的秒数间隔，0为不启用-->
    <add key="loadDelayTaskIntervalSeconds" value="5"/>
    <!--是否记录调用成功的日志，0-不记录，1-记录-->
    <add key="recordSuccessLog" value="0"/>
  </appSettings>
```