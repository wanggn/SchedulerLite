﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerLite.Client
{
    public class DelayTask
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 请求地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// POST / GET
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// POST数据
        /// </summary>
        public string PostData { get; set; }

        /// <summary>
        /// 触发时间
        /// </summary>
        public DateTime TriggerTime { get; set; }

        /// <summary>
        /// 最大重试次数，0无限，-1不重试
        /// </summary>
        public int MaxRetryCount { get; set; } = -1;

        /// <summary>
        /// 重试次数
        /// </summary>
        public int RetryCount { get; set; } = 3;
        /// <summary>
        /// 不成功时，重试间隔秒数
        /// </summary>
        public int RetrySeconds { get; set; } = 30;

        /// <summary>
        /// 超时秒数
        /// </summary>
        public int TimeoutSeconds { get; set; } = 15;
        /// <summary>
        /// 成功标识
        /// </summary>
        public string SuccessFlag { get; set; } = "";

    }
}
