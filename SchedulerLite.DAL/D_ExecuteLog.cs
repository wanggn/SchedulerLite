﻿using Loogn.OrmLite;
using SchedulerLite.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchedulerLite.DAL
{
    public class D_ExecuteLog
    {
        public static long Add(ExecuteLog m)
        {
            using (var db = DB.Open())
            {
                return db.Insert(m);
            }
        }

        public static OrmLitePageResult<ExecuteLog> SearchList(long taskId, int taskType, int status, int pageIndex, int pageSize)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("1=1");
            if (taskId > 0)
            {
                sb.AppendFormat(" and TaskId={0}", taskId);
            }
            if (taskType > 0)
            {
                sb.AppendFormat(" and TaskType={0}", taskType);
            }
            if (status > 0)
            {
                sb.AppendFormat(" and Status={0}", status);
            }
            using (var db = DB.Open())
            {
                return db.SelectPage<ExecuteLog>(new OrmLitePageFactor
                {
                    Conditions = sb.ToString(),
                    OrderBy = "id desc",
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                });
            }
        }
    }
}
